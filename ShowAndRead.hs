-- Austin Lee and Karl Menzel
-- Print and Read Functions for Chess.hs

module ShowAndRead (
   fileName,
   showCoord,
   showArsenal,
   showResult,
   showPlayer,
   showBoard,
   showMove,
   showPieceType,
   showPiece,
   writeGame,
   testRead,
   readGame,
   readBoard,
   readArsenal,
   readPiece,
   readCoord,
   readPieceType,
   readPlayer
) where

--import Data.Text
import Data.Maybe
import Data.List.Split
import System.IO
import KarlChess

fileName = ".game_in_progress.txt"

---- Shows
showCoord :: Coord -> String
showCoord (col,row) = (col:[]) ++ "'" ++ (show row)

showArsenal :: Arsenal -> String
showArsenal (p:[]) = showPiece p
showArsenal (p:ps) = showPiece p ++ "/" ++ showArsenal ps

showResult :: Result -> String
showResult result =
   case result of
      Won player -> showPlayer player
      Tie -> "Nothing"

showPlayer :: Player -> String
showPlayer player =
   case player of
      White -> "White"
      Black -> "Black"

showBoard :: Board -> String
showBoard board = showArsenal (white board) ++ "\n" ++ showArsenal (black board) ++ "\n" ++ showPlayer (active board)

showMove :: Move -> String
showMove move = "(" ++ showPiece (from move) ++ " " ++ showCoord (to move) ++ ")"

showPieceType :: PieceType -> String
showPieceType pieceType =
   case pieceType of
      King -> "King"
      Pawn -> "Pawn"
      Rook -> "Rook"
      Queen -> "Queen"
      Bishop -> "Bishop"
      Knight -> "Knight"

showPiece :: Piece -> String
showPiece piece = showPieceType (kind piece) ++ " " ++ showPlayer (color piece) ++ " " ++ showCoord (location piece) ++ " " ++ show (hasNotMoved piece)

writeGame :: Board -> IO ()
writeGame brd = do writeFile fileName (showBoard brd)
    

---- Reads

testRead = "Rook White A'1/Knight White B'1/Bishop White C'1/Queen White D'1/King White E'1/Bishop White F'1/Knight White G'1/Rook White H'1/Pawn White A'2/Pawn White B'2/Pawn White C'2/Pawn White D'2/Pawn White E'2/Pawn White F'2/Pawn White G'2/Pawn White H'2\nRook Black A'8/Knight Black B'8/Bishop Black C'8/Queen Black D'8/King Black E'8/Bishop Black F'8/Knight Black G'8/Rook Black H'8/Pawn Black A'7/Pawn Black B'7/Pawn Black C'7/Pawn Black D'7/Pawn Black E'7/Pawn Black F'7/Pawn Black G'7/Pawn Black H'7\nWhite"

readGame :: String -> IO (Maybe Board)
readGame fileName = do
    gameFile <- openFile fileName ReadMode
    gameText <- hGetContents gameFile 
    return $ readBoard [gameText]

readBoard :: [String] -> Maybe Board
readBoard (whiteArsenalStr:blackArsenalStr:activeStr:[]) = do 
    whiteArsenal <- readArsenal whiteArsenalStr
    blackArsenal <- readArsenal blackArsenalStr
    active <- readPlayer activeStr
    return (Board whiteArsenal blackArsenal active)
readBoard _ = Nothing

readArsenal :: String -> Maybe Arsenal
readArsenal str = 
    let pics = splitOn "/" str
    in Just $ catMaybes (map readPiece pics)

readPiece :: String -> Maybe Piece
readPiece str = 
    case words str of
         (picTypeStr:playerStr:locStr:hasntMovedStr:_)  -> do
            picType <- readPieceType picTypeStr
            player <- readPlayer playerStr
            locCoord <- readCoord locStr
            hasNotMoved <- readBool hasntMovedStr
            return (Piece picType player locCoord hasNotMoved)
         _ -> Nothing

readBool :: String -> Maybe Bool
readBool "True" = Just True
readBool "False" = Just False
readBool str = Nothing

readMove :: String -> Move
readMove str = undefined

readCoord :: String -> Maybe Coord
readCoord str = 
    case  (splitOn "'\'" str) of
        ([l]:num:[]) -> do n <- read num
                           return (l, n)
        _ -> Nothing

readPieceType :: String -> Maybe PieceType
readPieceType "King" = Just King
readPieceType "Pawn" = Just Pawn
readPieceType "Rook" = Just Rook
readPieceType "Queen" = Just Queen
readPieceType "Bishop" = Just Bishop
readPieceType "Knight" = Just Knight
readPieceType _ = Nothing

readPlayer :: String -> Maybe Player
readPlayer plr =
    case plr of
        "Black" -> Just Black
        "White" -> Just White
        otherwise -> Nothing
    
