
import KarlChess
import ShowAndRead
import Data.Char
import System.Console.GetOpt
import System.Environment

data Flag = Help | Winner | Depth Int | MakeMove Move | Interactive deriving (Eq, Show)

options :: [OptDescr Flag]
options = [
    Option ['h'] ["help"] (NoArg Help) "Show the help menu.",
    Option ['w'] ["winner"] (NoArg Winner) "Show who will win the game using an exhaustive search.",
    Option ['d'] ["depth"] (ReqArg (\str -> Depth (read str)) "num") "Use <num> as a cutoff depth for the search.",
    Option ['m'] ["move"] (ReqArg (\str -> MakeMove (readMove str)) "move") "Executes <move> and displays the resulting board.",
    Option ['i'] ["interactive"] (NoArg Interactive) "Play an interactive game."
    ]

readMove :: String -> Move
readMove = undefined

main :: IO ()
main = do
   args <- getArgs
   let (flags, others, errors) = getOpt Permute options args
   if Help `elem` flags
   then putStrLn $ usageInfo "Chess" options
   else if Winner `elem` flags
        then do
            ioGame <- (readGame ".game_in_progress.txt")
            case ioGame of
                Just board -> putStrLn $ show (whoWillWin board)
                otherwise -> putStrLn "Game not found."
   else if getDepth flags /= -1
        then putStrLn "TODO: Specific search"
   else if Interactive `elem` flags 
        then do
            (putStrLn "\nWelcome to Austin & Karl's Chess game!")
            (interactiveGame flags)
   else putStrLn "TODO: Write evaluate"

getDepth :: [Flag] -> Int
getDepth [] = -1
getDepth ((Depth x):xs) = x
getDepth (x:xs) = getDepth xs

interactiveGame :: [Flag] -> IO ()
interactiveGame flags = do
   putStrLn "Who will be white?"
   whitePlayer <- getLine
   putStrLn "Who will be black?"
   blackPlayer <- getLine
   putStrLn "Would you like to continue the previous game?"
   continueRawResponse <- getLine
   let continueResponse = head $ map toLower continueRawResponse
   if (continueResponse == 'y')
   then do
      ioGame <- (readGame ".game_in_progress.txt")
      case ioGame of
         Just board -> chessLoop board whitePlayer blackPlayer
         otherwise -> putStrLn "Previous game not found."
   else chessLoop startGame whitePlayer blackPlayer
   putStr "Would you like to play another game? (Enter 'no' if not)  "
   again <- getLine
   let againResponse = head $ map toLower again
   if (againResponse == 'y')
   then return ()
   else main

chessLoop :: Board -> String -> String -> IO ()
chessLoop board w b = do
   printBoard board
   putStr "It's "
   case active board of
      White -> putStr w
      Black -> putStr b
   putStrLn "'s turn!\n" 
   move <- moveLoop board
   if (checkMate board (currentValidMoves board))
   then return ()
   else do
      let boardI = executeMove board move
      writeGame boardI
      chessLoop boardI w b


moveLoop :: Board -> IO Move
moveLoop board = do
   putStrLn "Move piece at:"
   piece <- pieceLoop board
   putStrLn "To:"
   coord <- coordLoop
   if ((Move piece coord) `elem` (currentValidMoves board))
   then return $ Move piece coord
   else do
      putStrLn "Invalid move.\nTry again."
      moveLoop board


pieceLoop :: Board -> IO Piece
pieceLoop board = do
   coord <- getCoord
   if (validLocation coord)
   then case checkBoardLocation board coord of
      Just piece -> return $ piece
      Nothing -> do
         putStrLn "   You do not have a piece at that location.\n   Try again."
         pieceLoop board
   else do
      putStrLn "   Invalid coordinate.\n   Try again."
      pieceLoop board

coordLoop :: IO Coord
coordLoop = do
   coord <- getCoord
   if (validLocation coord)
   then return $ coord
   else do
      putStrLn "   Invalid coordinate.\n   Try again."
      coordLoop

getFile :: IO Char
getFile = do
   putStr "      Enter a file: "
   ch <- getLine
   return $ toUpper (head ch)

getRank :: IO Int
getRank = putStr "      Enter a rank: " >> getLine >>= return . read

getCoord :: IO Coord
getCoord = do
   putStrLn "   Enter a coordinate:"
   file <- getFile
   rank <- getRank
   return $ (file, rank)
