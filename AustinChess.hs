--  Chess.hs
--  Karl Menzel & Austin Lee
--  Written for Principles of Functional Languages
--  Taught by Dr. Seth Fogarty
--
--
-- TODO: Move counter


module AustinChess (
   Coord(..),
   Arsenal(..),
   Result(..),
   Player(..),
   Board(..),
   Move(..),
   PieceType(..),
   Piece(..),
   startGame,
   initialWhite,
   initialBlack,
   winner,
   validLocation,
   checkBoardLocation,
   charAdd,
   currentValidMoves,
   validMoves,
   possibleMoveCoords,
   pawnCaptureMoves,
   pathUnobstructed,
   canMoveTo,
   executeMove,
   removePieceFromBoard,
   opponent,
   removePieceFromArsenal,
   check,
   checkMate,
   kingLocation,
   activeArsenal,
   opponentArsenal,
   printBoard,
   rowLoop,
   colLoop,
   pieceToString
) where


import Data.Char


-- Types
type Coord = (Char, Int)
type Arsenal = [Piece]
data Result = Won Player | Tie deriving Show
data Player = White | Black deriving (Eq, Show)
data Board = Board { white :: Arsenal, black :: Arsenal, active :: Player }
data Move = Move { from :: Piece, to :: Coord } deriving (Eq, Show)
data PieceType = King | Pawn | Rook | Queen | Bishop | Knight deriving (Eq, Show)
data Piece = Piece { kind :: PieceType, color :: Player, location :: Coord, hasNotMoved :: Bool } deriving (Eq, Show)

instance Enum PieceType where
   fromEnum King = 0
   fromEnum Pawn = 1
   fromEnum Knight = 3
   fromEnum Bishop = 4
   fromEnum Rook = 5
   fromEnum Queen = 9

   toEnum 0 = King
   toEnum 1 = Pawn
   toEnum 3 = Knight
   toEnum 4 = Bishop
   toEnum 5 = Rook
   toEnum 9 = Queen
   

-- Setup
startGame = Board initialWhite initialBlack White

initialWhite = [ Piece Rook White ('A', 1) True, Piece Knight White ('B',1) True, Piece Bishop White ('C',1) True, Piece Queen White ('D',1) True,
                  Piece King White ('E',1) True, Piece Bishop White ('F',1) True, Piece Knight White ('G',1) True, Piece Rook White ('H',1) True,
                  Piece Pawn White ('A',2) True, Piece Pawn White ('B',2) True, Piece Pawn White ('C',2) True, Piece Pawn White ('D',2) True,
                  Piece Pawn White ('E',2) True, Piece Pawn White ('F',2) True, Piece Pawn White ('G',2) True, Piece Pawn White ('H',2) True ]

initialBlack = [ Piece Rook Black ('A', 8) True, Piece Knight Black ('B',8) True, Piece Bishop Black ('C',8) True, Piece Queen Black ('D',8) True,
                  Piece King Black ('E',8) True, Piece Bishop Black ('F',8) True, Piece Knight Black ('G',8) True, Piece Rook Black ('H',8) True,
                  Piece Pawn Black ('A',7) True, Piece Pawn Black ('B',7) True, Piece Pawn Black ('C',7) True, Piece Pawn Black ('D',7) True,
                  Piece Pawn Black ('E',7) True, Piece Pawn Black ('F',7) True, Piece Pawn Black ('G',7) True, Piece Pawn Black ('H',7) True ]


-- Winner
-- | moves board == 0 = Tie
-- add ^^ to body after implementing move counting 
winner :: Board -> Maybe Result
winner board
   | checkMate board (currentValidMoves board) = Just $ Won (opponent board)
   | checkMate oppBoard (currentValidMoves oppBoard) = Just $ Won (active board)
   | otherwise = Nothing
   where
      oppBoard = board { active = opponent board }


-- Location Checks
validLocation :: Coord -> Bool
validLocation (col, row)
   | col < 'A' = False
   | col > 'H' = False
   | row < 1 = False
   | row > 8 = False
   | otherwise = True

checkBoardLocation :: Board -> Coord -> Maybe Piece
checkBoardLocation board coord
   | validLocation coord = checkPlayerLocation (white board ++ black board) coord
   | otherwise = Nothing
   where
      checkPlayerLocation arsenal coord = foldl (\z piece -> if (location piece) == coord then Just piece else z) Nothing arsenal


charAdd :: Char -> Int -> Char
charAdd col n = chr (ord col + n)

-- Finding Moves
currentValidMoves :: Board -> [Move]
currentValidMoves board = filter (\m -> not (check ((executeMove board m) { active = active board }))) $ foldl (\z piece -> (validMoves board piece) ++ z) [] (activeArsenal board)

validMoves :: Board -> Piece -> [Move]
validMoves board piece =
   case kind piece of
      Knight -> filter (\m -> canMoveTo board (to m)) $ map (\coord -> (Move piece coord)) (filter validLocation (possibleMoveCoords piece))
      King -> (castleMoves board piece) ++ (filter (pathUnobstructed board) (map (\coord -> (Move piece coord)) (filter validLocation (possibleMoveCoords piece))))
      Pawn -> (pawnCaptureMoves board piece) ++ (filter (pathUnobstructed board) (map (\coord -> (Move piece coord)) (filter (\coord -> validLocation coord && checkBoardLocation board coord == Nothing) (possibleMoveCoords piece))))
      _ -> filter (pathUnobstructed board) $ map (\coord -> (Move piece coord)) (filter validLocation (possibleMoveCoords piece))

possibleMoveCoords :: Piece -> [Coord]
possibleMoveCoords (Piece King c (col,row) notMoved) = [ (cl,rw) | cl <- [charAdd col (-1) ..charAdd col 1], rw <- [(row - 1)..(row + 1)], cl /= col || rw /= row ]
possibleMoveCoords (Piece Rook c (col,row) notMoved) = [ (cl,row) | cl <- ['A'..'H'], cl /= col ] ++ [ (col,rw) | rw <- [1..8], rw /= row ]
possibleMoveCoords (Piece Queen c (col,row) notMoved) = possibleMoveCoords (Piece Rook c (col,row) notMoved) ++ possibleMoveCoords (Piece Bishop c (col,row) notMoved)
possibleMoveCoords (Piece Bishop c (col,row) notMoved) = [ (charAdd col n, row + n) | n <- [-7..7], n /= 0 ] ++ [ (charAdd col n, row - n) | n <- [-7..7], n /= 0 ]
possibleMoveCoords (Piece Knight c (col,row) notMoved) = [ (charAdd col (m*x), row + (n*y)) | (x,y) <- [(1,2), (2,1)], m <- [-1,1], n <- [-1,1] ]
possibleMoveCoords (Piece Pawn c (col,row) notMoved) = 
   case c of
      White -> [ (col, row + 1) ] ++ if (notMoved) then [ (col, row + 2) ] else []
      Black -> [ (col, row - 1) ] ++ if (notMoved) then [ (col, row - 2) ] else []


-- Allow pawn to capture diagonally
pawnCaptureMoves :: Board -> Piece -> [Move]
pawnCaptureMoves board piece =
   case color piece of
      White -> aux board piece 1
      Black -> aux board piece (-1)
   where
      aux :: Board -> Piece -> Int -> [Move]
      aux board piece r =
         case checkBoardLocation board (charAdd col 1, row + r) of
            Just p -> if color p == opponent board then [ (Move piece (charAdd col 1, row + r)) ] else []
            Nothing -> []
         ++ case checkBoardLocation board (charAdd col (-1), row + r) of
            Just p -> if color p == opponent board then [ (Move piece (charAdd col (-1), row + r)) ] else []
            Nothing -> []
         where
            (col,row) = location piece


-- Allow for castling
castleMoves :: Board -> Piece -> [Move]
castleMoves board piece
   | hasNotMoved piece = (if (canCastle board ('A',row) 1) then [ (Move piece (charAdd col (-2), row)) ] else []) ++ (if (canCastle board ('H',row) (-1)) then [ (Move piece (charAdd col (2), row)) ] else [])
   | otherwise = []
   where
      (col,row) = location piece
      canCastle :: Board -> Coord -> Int -> Bool
      canCastle board coord i = 
         case checkBoardLocation board coord of
            Just p -> kind p == Rook && hasNotMoved p && pathUnobstructed board (Move piece (charAdd (fst (location p)) i, row))
            Nothing -> False


-- Check if a piece can traverse path to new location without jumping any pieces
pathUnobstructed :: Board -> Move -> Bool
pathUnobstructed board move
   | toCol > fromCol && toRow > fromRow = validPath board (1,1) (charAdd fromCol 1, fromRow + 1) (to move)
   | toCol > fromCol && toRow == fromRow = validPath board (1,0) (charAdd fromCol 1, fromRow) (to move)
   | toCol > fromCol && toRow < fromRow = validPath board (1,-1) (charAdd fromCol 1, fromRow - 1) (to move)
   | toCol == fromCol && toRow < fromRow = validPath board (0,-1) (fromCol, fromRow - 1) (to move)
   | toCol == fromCol && toRow > fromRow = validPath board (0,1) (fromCol, fromRow + 1) (to move)
   | toCol < fromCol && toRow > fromRow = validPath board (-1,1) (charAdd fromCol (-1), fromRow + 1) (to move)
   | toCol < fromCol && toRow == fromRow = validPath board (-1,0) (charAdd fromCol (-1), fromRow) (to move)
   | toCol < fromCol && toRow < fromRow = validPath board (-1,-1) (charAdd fromCol (-1), fromRow - 1) (to move)
   where
      (toCol,toRow) = to move
      (fromCol,fromRow) = location (from move)
      validPath :: Board -> (Int,Int) -> Coord -> Coord -> Bool
      validPath board (c,r) (col,row) coord
         | (col,row) == coord = canMoveTo board coord
         | otherwise = (checkBoardLocation board (col,row) == Nothing) && validPath board (c,r) (charAdd col c, row + r) coord

canMoveTo :: Board -> Coord -> Bool
canMoveTo board coord =
   case checkBoardLocation board coord of
      Just piece -> if color piece == active board then False else True
      Nothing -> True


-- Executing Moves
executeMove :: Board -> Move -> Board
executeMove board move
   | kind (from move) == King && fst (location (from move)) == charAdd (fst (to move)) 2 = executeCastleMove board move 1
   | kind (from move) == King && fst (location (from move)) == charAdd (fst (to move)) (-2) = executeCastleMove board move (-1)
   | otherwise = let boardI = removePieceFromBoard (active board) board (location (from move))
                     boardII = removePieceFromBoard (opponent boardI) boardI (to move)
                 in case active boardII of
                    White -> Board ((from move) { location = to move, hasNotMoved = False } : white boardII) (black boardII) (opponent boardII)
                    Black -> Board (white boardII) ((from move) { location = to move, hasNotMoved = False } : black boardII) (opponent boardII)

executeCastleMove :: Board -> Move -> Int -> Board
executeCastleMove board move i =
   let boardI = removePieceFromBoard (active board) board (location rook)
       boardII = removePieceFromBoard (active boardI) boardI (location (from move))
   in case active boardII of
      White -> Board ((from move) { location = to move, hasNotMoved = False } : rook { location = (charAdd toCol i, row), hasNotMoved = False } : white boardII) (black boardII) (opponent boardII)
      Black -> Board (white boardII) ((from move) { location = to move, hasNotMoved = False } : rook { location = (charAdd toCol i, row), hasNotMoved = False } : black boardII) (opponent boardII)
   where
      (fromCol,row) = location (from move)
      toCol = fst (to move)
      leftRook = Piece Rook (color (from move)) ('A',row) True
      rightRook = Piece Rook (color (from move)) ('H',row) True
      rook = if (i < 0) then rightRook else leftRook
            
removePieceFromBoard :: Player -> Board -> Coord -> Board
removePieceFromBoard player (Board w b a) coord =
   case player of
      White -> Board (removePieceFromArsenal w coord) b a
      Black -> Board w (removePieceFromArsenal b coord) a

opponent :: Board -> Player
opponent board =
   case active board of
      White -> Black
      Black -> White

removePieceFromArsenal :: Arsenal -> Coord -> Arsenal
removePieceFromArsenal arsenal coord = filter (\x -> (location x) /= coord) arsenal


-- Check(Mate)
check :: Board -> Bool
check board = 
   case kingLocation board of
      Just king -> any (\m -> to m == king) (currentValidMoves (board { active = opponent board }))
      Nothing -> True

checkMate :: Board -> [Move] -> Bool
checkMate board [] = True
checkMate board (m:ms) = check ((executeMove board m) { active = active board }) && checkMate board ms

kingLocation :: Board -> Maybe Coord
kingLocation board = foldl (\z piece -> if kind piece == King then Just (location piece) else z) Nothing (activeArsenal board)

-- Change name of function
activeArsenal :: Board -> Arsenal
activeArsenal board =
   case active board of
      White -> white board
      Black -> black board
-------

opponentArsenal :: Board -> Arsenal
opponentArsenal board =
   case active board of
      White -> black board
      Black -> white board


-- Print
printBoard :: Board -> IO ()
printBoard board = rowLoop board 9

rowLoop :: Board -> Int -> IO ()
rowLoop board row = do
   let ret = (show row) ++ "  "
   if (row > 0) then do
      if (row == 9) then
         putStrLn ""
      else
         putStrLn (colLoop board 'A' row ret)
      rowLoop board (row - 1)
   else
      putStrLn "\n   A  B  C  D  E  F  G  H\n"

colLoop :: Board -> Char -> Int -> String -> String
colLoop board col row ret = do
   if (col < 'I') then
      case checkBoardLocation board (col,row) of
         Just p -> colLoop board (charAdd col 1) row (ret ++ (pieceToString p) ++ " ")
         Nothing -> colLoop board (charAdd col 1) row (ret ++ ("-- "))
   else
      ret

pieceToString :: Piece -> String
pieceToString piece = (map toLower (take 1 (show (color piece)))) ++
   case kind piece of
      King -> "K" -- +
      Pawn -> "P" -- i
      Rook -> "R" -- #
      Queen -> "Q" -- *
      Bishop -> "B" -- ^
      Knight -> "?"
{-
if color piece == White then "&#9812;" else "&#9818;"    -- 
if color piece == White then "U+2659" else "&#9823;"    -- 
if color piece == White then "&#9814;" else "&#9820;"    -- 
if color piece == White then "&#9813;" else "&#9819;"      -- 
if color piece == White then "&#9815;" else "&#9821;"     -- 
if color piece == White then "&#9816;" else "&#9822;"     -- 
-}

-- defeatedPieces = Player []
arsenalScore :: Arsenal -> Int
arsenalScore arsenal = foldl (\z piece -> fromEnum (kind piece) + z) 0 arsenal

-- Add weight for a castling move?
-- Can only call with even depth. ---
minimax :: Board -> Int -> Move -> (Int, Move)
minimax board 0 = (0, Move (Piece Pawn Black ('I',9) False)
minimax board depth
   | depth `mod` 2 == 0 = let weights = weightedMoves (+) in bestMove weights $ maximum (map (\im -> fst im) weights)
   | otherwise = let weights = weightedMoves (-) in bestMove weights $ minimum (map (\im -> fst im) weights)
   where
      weightedMoves :: (Int -> Int -> Int) -> [(Int, Move)]
      weightedMoves f = foldl (\z move -> (weight board f move) : z) [] (currentValidMoves board) 
      bestMove :: [(Int,Move)] -> Int -> (Int, Move)
      bestMove (im:ims) i = if fst im == i then im else bestMove ims i
      weight :: Board -> (Int -> Int -> Int) -> Move -> (Int, Move)
      weight board f move
         | checkMate newBoard (currentValidMoves newBoard) = (100, move)
         | check newBoard = (f (fst (minimax newBoard (depth - 1))) 10, move)
         | scoreDiff > 0 = (f (fst (minimax newBoard (depth - 1))) scoreDiff, move)
         | otherwise = (fst (minimax newBoard (depth - 1)), move)
         where
            newBoard = executeMove board move
            scoreDiff = arsenalScore (opponentArsenal board) - arsenalScore (activeArsenal newBoard)


{-
optimalMove :: Board -> Player -> Int -> Move
optimalMove board play i = foldl (\z m -> minimax board m i : z) [] (currentValidMoves board)
   where
      minimax :: Board -> Player -> Move -> Int -> Int
      minimax board play move i
         | checkMate board (currentValidMoves board) = if play == active board then 3 else -3
         | check board = if play == active board then 2 else -2
         | pieceTaken board move = if play == active board then 1 else -1
         | otherwise = 0
         where
            pieceTaken :: Board -> Move -> Bool
            pieceTaken board move = length (activeArsenal (board { active = opponent board })) > length (activeArsenal (executeMove board move))
-}
